use array2d::Array2D;
use criterion::{criterion_group, criterion_main, Criterion};
use morpion::game::Game;
use morpion::morpion::game::MorpionGame;
use morpion::morpion::general::MorpionAction;

fn win(n: usize) {
    let c = 'X';
    let board_data = Array2D::filled_with(c, n, n).as_row_major();
    let mut game = MorpionGame::load(n, n, board_data, (n - 1) as u8, vec![1]);
    let action = MorpionAction {
        position: (n / 2, n / 2),
        board_box: c,
    };
    game.win(&action);
}

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("win_5", |b| b.iter(|| win(5)));
    c.bench_function("win_100", |b| b.iter(|| win(100)));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
