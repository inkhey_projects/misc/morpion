use crate::game::Game;
use crate::morpion::board::MorpionBoard;
use crate::morpion::general::{MorpionAction, MorpionError, PlayerChar};
use crate::users::{Turn, UserId};
use serde::{Deserialize, Serialize};

pub(super) const EMPTY_ROW: char = '·';

#[derive(Serialize, Deserialize, Debug)]
pub struct MorpionGame {
    board: MorpionBoard<PlayerChar>,
    turn: Turn<UserId>,
    winning_row_length: u8,
}

impl MorpionGame {
    pub fn new(
        column_nb: usize,
        row_nb: usize,
        winning_row_length: u8,
        players: Vec<UserId>,
    ) -> Self {
        MorpionGame {
            board: MorpionBoard::new(column_nb, row_nb, EMPTY_ROW),
            turn: Turn::new(players),
            winning_row_length,
        }
    }

    pub fn load(
        column_nb: usize,
        row_nb: usize,
        board_data: Vec<PlayerChar>,
        winning_row_length: u8,
        players: Vec<UserId>,
    ) -> Self {
        MorpionGame {
            board: MorpionBoard::load(column_nb, row_nb, EMPTY_ROW, board_data),
            turn: Turn::new(players),
            winning_row_length,
        }
    }
    pub fn get_current_player(&self) -> UserId {
        self.turn.current()
    }

    pub fn get_next_player(&mut self) -> UserId {
        self.turn.next()
    }
}

impl Game<MorpionAction<PlayerChar>, MorpionError<PlayerChar>> for MorpionGame {
    fn play(&mut self, action: &MorpionAction<PlayerChar>) -> Result<(), MorpionError<PlayerChar>> {
        self.board.set_value(action.position, action.board_box)
    }

    fn win(&self, last_action: &MorpionAction<PlayerChar>) -> bool {
        self.board
            .check_win(last_action.position, self.winning_row_length)
    }

    fn str(&self) -> String {
        self.board.str()
    }
}

#[cfg(test)]
mod tests {
    use crate::game::Game;
    use crate::morpion::game::MorpionGame;
    use crate::morpion::general::MorpionAction;
    use array2d::Array2D;

    #[test]
    fn test_play_nominal_case() {
        let rows = vec![
            //    0    1    2
            vec!['·', '·', 'O'], // 0
            vec!['·', 'X', '·'], // 1
            vec!['·', 'X', '·'], // 2
        ];
        let players_id = vec![1, 2];
        let array = Array2D::from_rows(&rows).unwrap().as_row_major();
        let mut board = MorpionGame::load(3, 3, array, 3, players_id);
        let action_1 = MorpionAction {
            position: (0, 0),
            board_box: 'O',
        };
        let action_2 = MorpionAction {
            position: (0, 1),
            board_box: 'X',
        };

        assert_eq!(board.play(&action_1).unwrap(), ());
        assert_eq!(board.play(&action_2).unwrap(), ());
        assert_eq!(board.win(&action_1), false);
        assert_eq!(board.win(&action_2), true);
    }
}
