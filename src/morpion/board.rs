use crate::morpion::general::Dimension;
use crate::morpion::general::MorpionError::{OccupiedPosition, OutsideOfBoard};
use crate::morpion::general::{MorpionBoardBox, MorpionError};
use array2d::Array2D;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub(super) struct MorpionBoard<C: Copy> {
    board: Array2D<C>,
    empty_val: C,
}

const CHAR_SEP: &str = "\t";
const NEWLINE_SEP: &str = "\n";

type Way = (i32, i32);

const RIGHT: Way = (0, 1);
const DOWN: Way = (1, 0);
const LEFT: Way = (0, -1);
const UP: Way = (-1, 0);
const DIAG_UP_LEFT: Way = (-1, -1);
const DIAG_UP_RIGHT: Way = (-1, 1);
const DIAG_DOWN_LEFT: Way = (1, -1);
const DIAG_DOWN_RIGHT: Way = (1, 1);

type Direction = (Way, Way);

const VERTICAL: Direction = (UP, DOWN);
const HORIZONTAL: Direction = (LEFT, RIGHT);
const DIAG_DOWN: Direction = (DIAG_UP_LEFT, DIAG_DOWN_RIGHT);
const DIAG_UP: Direction = (DIAG_DOWN_LEFT, DIAG_UP_RIGHT);

impl<C: MorpionBoardBox> MorpionBoard<C> {
    pub(super) fn new(column_nb: usize, row_nb: usize, empty_val: C) -> Self {
        MorpionBoard {
            board: Array2D::filled_with(empty_val, column_nb, row_nb),
            empty_val,
        }
    }

    pub(super) fn load(column_nb: usize, row_nb: usize, empty_val: C, board_data: Vec<C>) -> Self {
        MorpionBoard {
            board: Array2D::from_row_major(board_data.as_slice(), row_nb, column_nb).unwrap(),
            empty_val,
        }
    }

    pub(super) fn get_value(&self, position: Dimension) -> Result<C, MorpionError<C>> {
        match self.check_position(position) {
            false => Err(OutsideOfBoard(position)),
            true => Ok(self.board[position]),
        }
    }

    pub(super) fn set_value(
        &mut self,
        position: Dimension,
        new_value: C,
    ) -> Result<(), MorpionError<C>> {
        match self.get_value(position) {
            Ok(v) if v == self.empty_val => {
                self.board[position] = new_value;
                Ok(())
            }
            Ok(current_value) => Err(OccupiedPosition(position, current_value)),
            Err(e) => Err(e),
        }
    }

    pub(super) fn check_win(&self, position: Dimension, winning_row_length: u8) -> bool {
        self.check_win_direction(position, HORIZONTAL, winning_row_length)
            || self.check_win_direction(position, VERTICAL, winning_row_length)
            || self.check_win_direction(position, DIAG_UP, winning_row_length)
            || self.check_win_direction(position, DIAG_DOWN, winning_row_length)
    }

    pub(super) fn str(&self) -> String {
        // Header
        let mut header = vec![];
        for number in 0..(self.board.num_columns()) {
            header.push(number.to_string());
        }
        let mut res_string = format!("{CHAR_SEP}/{CHAR_SEP}");
        res_string.push_str(&header.join(CHAR_SEP));
        res_string.push_str(NEWLINE_SEP);

        // Rows
        for (i, row_iter) in self.board.rows_iter().enumerate() {
            res_string.push_str(CHAR_SEP);
            let mut row_data = vec![i.to_string()];
            for element in row_iter {
                row_data.push(element.to_string());
            }
            res_string.push_str(&row_data.join(CHAR_SEP));
            res_string.push_str(NEWLINE_SEP);
        }
        res_string
    }
}

impl<C: MorpionBoardBox> MorpionBoard<C> {
    fn check_win_direction(
        &self,
        position: Dimension,
        direction: Direction,
        winning_row_length: u8,
    ) -> bool {
        self.count_consecutive_in_direction(position, direction) >= winning_row_length
    }

    fn count_consecutive_in_direction(&self, position: Dimension, direction: Direction) -> u8 {
        let res = self.count_consecutive_in_way(position, direction.0)
            + self.count_consecutive_in_way(position, direction.1)
            + 1;
        match res {
            res if res >= 2 => res,
            _ => 0,
        }
    }

    fn count_consecutive_in_way(&self, position: Dimension, way: Way) -> u8 {
        let mut current_pos = position;
        let player = self.board[current_pos];
        if player == self.empty_val {
            return 0;
        }
        let mut consecutive: u8 = 0;
        let mut new_position;
        loop {
            new_position = self.valid_position(current_pos, way);
            match new_position {
                Some(pos) => {
                    if self.check_position(pos) {
                        if self.board[pos] == player {
                            consecutive += 1;
                            current_pos = pos;
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                None => break,
            }
        }
        consecutive
    }

    fn check_position(&self, position: Dimension) -> bool {
        (position.0 < self.board.num_rows()) && (position.1 < self.board.num_columns())
    }

    fn valid_position(&self, origin_value: Dimension, pos: Way) -> Option<Dimension> {
        Some((
            safe_add(origin_value.0, pos.0)?,
            safe_add(origin_value.1, pos.1)?,
        ))
    }
}

fn safe_add(a: usize, b: i32) -> Option<usize> {
    match a as i32 + b {
        num if num < 0 => None,
        num => Some(num as usize),
    }
}

#[cfg(test)]
mod tests {
    use crate::morpion::board::{
        safe_add, MorpionBoard, DIAG_DOWN, DIAG_DOWN_LEFT, DIAG_DOWN_RIGHT, DIAG_UP, DIAG_UP_LEFT,
        DIAG_UP_RIGHT, DOWN, HORIZONTAL, LEFT, RIGHT, UP, VERTICAL,
    };
    use crate::morpion::game::EMPTY_ROW;
    use array2d::Array2D;

    #[test]
    fn test_str() {
        let rows = vec![
            //    0    1    2
            vec!['·', '·', 'O'], // 0
            vec!['·', 'X', '·'], // 1
            vec!['·', 'X', '·'], // 2
        ];
        let board = Array2D::from_rows(&rows).unwrap();
        let game = MorpionBoard {
            board,
            empty_val: EMPTY_ROW,
        };
        assert_eq!(
            game.str(),
            "\t/\t0\t1\t2\n\t0\t·\t·\tO\n\t1\t·\tX\t·\n\t2\t·\tX\t·\n"
        )
    }

    #[test]
    fn test_count_consecutive_in_way() {
        let rows = vec![
            //    0    1    2    3    4    5
            vec!['X', 'Y', 'O', '·', '·', '·'], // 0
            vec!['Y', 'X', 'O', '·', '·', '·'], // 1
            vec!['·', 'O', 'O', 'O', '·', '·'], // 2
            vec!['·', '·', 'O', 'X', '·', '·'], // 3
            vec!['C', '·', 'O', 'C', 'X', '·'], // 4
        ];
        let array = Array2D::from_rows(&rows).unwrap();
        let board = MorpionBoard {
            board: array,
            empty_val: EMPTY_ROW,
        };

        let position_center_x = (2, 2);
        assert_eq!(board.count_consecutive_in_way(position_center_x, UP), 2);
        assert_eq!(board.count_consecutive_in_way(position_center_x, DOWN), 2);
        assert_eq!(board.count_consecutive_in_way(position_center_x, LEFT), 1);
        assert_eq!(board.count_consecutive_in_way(position_center_x, RIGHT), 1);
        assert_eq!(
            board.count_consecutive_in_way(position_center_x, DIAG_UP_LEFT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_center_x, DIAG_DOWN_RIGHT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_center_x, DIAG_DOWN_LEFT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_center_x, DIAG_UP_RIGHT),
            0
        );

        let position_border_c = (4, 0);
        assert_eq!(board.count_consecutive_in_way(position_border_c, UP), 0);
        assert_eq!(board.count_consecutive_in_way(position_border_c, DOWN), 0);
        assert_eq!(board.count_consecutive_in_way(position_border_c, LEFT), 0);
        assert_eq!(board.count_consecutive_in_way(position_border_c, RIGHT), 0);
        assert_eq!(
            board.count_consecutive_in_way(position_border_c, DIAG_UP_LEFT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_border_c, DIAG_DOWN_RIGHT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_border_c, DIAG_DOWN_LEFT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_border_c, DIAG_UP_RIGHT),
            0
        );

        let position_diag_x = (3, 3);
        assert_eq!(board.count_consecutive_in_way(position_diag_x, UP), 0);
        assert_eq!(board.count_consecutive_in_way(position_diag_x, DOWN), 0);
        assert_eq!(board.count_consecutive_in_way(position_diag_x, LEFT), 0);
        assert_eq!(board.count_consecutive_in_way(position_diag_x, RIGHT), 0);
        assert_eq!(
            board.count_consecutive_in_way(position_diag_x, DIAG_UP_LEFT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_x, DIAG_DOWN_RIGHT),
            1
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_x, DIAG_DOWN_LEFT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_x, DIAG_UP_RIGHT),
            0
        );
        let position_diag_x_bis = (4, 4);
        assert_eq!(board.count_consecutive_in_way(position_diag_x_bis, UP), 0);
        assert_eq!(board.count_consecutive_in_way(position_diag_x_bis, DOWN), 0);
        assert_eq!(board.count_consecutive_in_way(position_diag_x_bis, LEFT), 0);
        assert_eq!(
            board.count_consecutive_in_way(position_diag_x_bis, RIGHT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_x_bis, DIAG_UP_LEFT),
            1
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_x_bis, DIAG_DOWN_RIGHT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_x_bis, DIAG_DOWN_LEFT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_x_bis, DIAG_UP_RIGHT),
            0
        );

        let position_diag_y = (1, 0);
        assert_eq!(board.count_consecutive_in_way(position_diag_y, UP), 0);
        assert_eq!(board.count_consecutive_in_way(position_diag_y, DOWN), 0);
        assert_eq!(board.count_consecutive_in_way(position_diag_y, LEFT), 0);
        assert_eq!(board.count_consecutive_in_way(position_diag_y, RIGHT), 0);
        assert_eq!(
            board.count_consecutive_in_way(position_diag_y, DIAG_UP_LEFT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_y, DIAG_DOWN_RIGHT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_y, DIAG_DOWN_LEFT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_y, DIAG_UP_RIGHT),
            1
        );
        let position_diag_y_bis = (0, 1);
        assert_eq!(board.count_consecutive_in_way(position_diag_y_bis, UP), 0);
        assert_eq!(board.count_consecutive_in_way(position_diag_y_bis, DOWN), 0);
        assert_eq!(board.count_consecutive_in_way(position_diag_y_bis, LEFT), 0);
        assert_eq!(
            board.count_consecutive_in_way(position_diag_y_bis, RIGHT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_y_bis, DIAG_UP_LEFT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_y_bis, DIAG_DOWN_RIGHT),
            0
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_y_bis, DIAG_DOWN_LEFT),
            1
        );
        assert_eq!(
            board.count_consecutive_in_way(position_diag_y_bis, DIAG_UP_RIGHT),
            0
        );
    }
    #[test]
    fn test_count_consecutive_in_direction() {
        let rows = vec![
            //    0    1    2
            vec!['·', '·', 'V'], // 0
            vec!['H', 'H', 'V'], // 1
            vec!['·', '·', 'V'], // 2
            vec!['·', '·', '·'], // 3
            vec!['D', 'U', '·'], // 4
            vec!['U', 'D', '·'], // 5
            vec!['·', '·', 'D'], // 6
        ];
        let board = Array2D::from_rows(&rows).unwrap();
        let game = MorpionBoard {
            board,
            empty_val: EMPTY_ROW,
        };
        for direction in vec![VERTICAL, HORIZONTAL, DIAG_UP, DIAG_DOWN] {
            for c in 0..game.board.num_columns() {
                for r in 0..game.board.num_rows() {
                    let res = game.count_consecutive_in_direction((r, c), direction);
                    let excepted = match (r, c, direction) {
                        // vertical Line of H
                        (r, 2, VERTICAL) if r <= 2 => 3,
                        // horizontal Line of V
                        (1, c, HORIZONTAL) if c <= 1 => 2,
                        // First diag (U)
                        (5, 0, DIAG_UP) => 2,
                        (4, 1, DIAG_UP) => 2,
                        // Second diag (D)
                        (4, 0, DIAG_DOWN) => 3,
                        (5, 1, DIAG_DOWN) => 3,
                        (6, 2, DIAG_DOWN) => 3,
                        (_, _, _) => 0,
                    };
                    assert_eq!(res, excepted, "'{},{}', for direction '{}-{}:{}-{}' give '{}' as result instead of '{}', {}",
                                   r, c,
                                   direction.0.0,
                                   direction.0.1,
                                   direction.0.1,
                                   direction.1.1,
                                   res,
                                   excepted,
                                   game.board[(r,c)]
                        );
                }
            }
        }
    }

    #[test]
    fn test_check_position() {
        let game = MorpionBoard::new(3, 3, EMPTY_ROW);
        assert_eq!(game.check_position((0, 0)), true);
        assert_eq!(game.check_position((2, 2)), true);
        assert_eq!(game.check_position((2, 0)), true);
        assert_eq!(game.check_position((1, 1)), true);
        assert_eq!(game.check_position((0, 2)), true);

        assert_eq!(game.check_position((3, 2)), false);
        assert_eq!(game.check_position((2, 3)), false);
        assert_eq!(game.check_position((4, 3)), false);

        let game = MorpionBoard::new(2, 5, EMPTY_ROW);
        assert_eq!(game.check_position((0, 0)), true);
        assert_eq!(game.check_position((1, 4)), true);
        assert_eq!(game.check_position((1, 1)), true);

        assert_eq!(game.check_position((2, 0)), false);
        assert_eq!(game.check_position((0, 5)), false);
        assert_eq!(game.check_position((2, 5)), false);
    }

    #[test]
    fn test_safe_add() {
        assert_eq!(safe_add(10, -10), Some(0));
        assert_eq!(safe_add(0, 1), Some(1));
        assert_eq!(safe_add(10, -11), None);
        assert_eq!(safe_add(0, -1), None)
    }
}
