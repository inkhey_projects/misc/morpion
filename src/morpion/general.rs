use std::fmt::Debug;
use trait_set::trait_set;

pub type PlayerChar = char;
pub type Dimension = (usize, usize);

trait_set! {
    pub trait MorpionBoardBox = Copy + Clone + PartialEq + ToString + Sync + Debug;
}

pub struct MorpionAction<C: MorpionBoardBox> {
    pub position: Dimension,
    pub board_box: C,
}

#[derive(Debug, PartialEq)]
pub enum MorpionError<C: MorpionBoardBox> {
    OutsideOfBoard(Dimension),
    OccupiedPosition(Dimension, C),
}

impl<C: MorpionBoardBox> MorpionError<C> {
    pub fn message(&self) -> &'static str {
        match self {
            MorpionError::OutsideOfBoard(_dimension) => "dimension is outside of board",
            MorpionError::OccupiedPosition(_dimension, ..) => "dimension is already occupied",
        }
    }
}
impl<C: MorpionBoardBox> std::fmt::Display for MorpionError<C> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error {}", self.message())
    }
}

impl<C: MorpionBoardBox> std::error::Error for MorpionError<C> {}
