use crate::game::Game;
use crate::morpion::game::MorpionGame;
use crate::morpion::general::{Dimension, MorpionAction, MorpionError, PlayerChar};
use crate::users::UserId;
use pyo3::create_exception;
use pyo3::prelude::*;
use pyo3::types::PyType;
use uuid::Uuid;

create_exception!(morpion.core, OutsideOfBoard, pyo3::exceptions::PyException);

create_exception!(morpion.core, AlreadyOccupied, pyo3::exceptions::PyException);

#[derive(Clone)]
#[pyclass(name = "UserId")]
pub(crate) struct PyUserId {
    uuid: UserId,
}

#[pymethods]
impl PyUserId {
    #[new]
    fn generate() -> Self {
        PyUserId {
            uuid: Uuid::new_v4().as_u128(), // Use v7 when possible
        }
    }

    #[classmethod]
    fn valid_values_range(_cls: &PyType) -> (u128, u128) {
        (
            Uuid::nil().as_u128(),
            Uuid::from_bytes([0xFF; 16]).as_u128(),
        ) // Use max when possible
    }

    #[classmethod]
    fn from_str(_cls: &PyType, serialized_str: &str) -> Self {
        PyUserId {
            uuid: Uuid::parse_str(serialized_str).unwrap().as_u128(),
        }
    }

    #[classmethod]
    fn from_u128(_cls: &PyType, serialized_u128: u128) -> Self {
        PyUserId {
            uuid: Uuid::from_u128(serialized_u128).as_u128(),
        }
    }
    fn __str__(&self) -> PyResult<String> {
        Ok(Uuid::from_u128(self.uuid).to_string())
    }

    fn __eq__(&self, other: Self) -> bool {
        self.uuid.eq(&other.uuid.clone())
    }
}

#[pyclass(name = "MorpionAction")]
pub struct PyMorpionAction {
    action: MorpionAction<char>,
}
#[pymethods]
impl PyMorpionAction {
    #[new]
    fn new(position: Dimension, board_box: char) -> Self {
        PyMorpionAction {
            action: MorpionAction {
                position,
                board_box,
            },
        }
    }
}

#[pyclass(name = "Morpion")]
pub struct PyMorpion {
    game: MorpionGame,
}

#[pymethods]
impl PyMorpion {
    #[new]
    fn new(
        column_nb: usize,
        row_nb: usize,
        winning_row_length: u8,
        py_players: Vec<PyUserId>,
        board_data: Option<Vec<PlayerChar>>,
    ) -> Self {
        let mut players: Vec<UserId> = Vec::new();
        for player in py_players {
            players.push(player.uuid)
        }
        let game = match board_data {
            None => MorpionGame::new(column_nb, row_nb, winning_row_length, players),
            Some(board_data) => {
                MorpionGame::load(column_nb, row_nb, board_data, winning_row_length, players)
            }
        };
        PyMorpion { game }
    }

    #[classmethod]
    fn load_str(_cls: &PyType, serialized_str: &str) -> Self {
        let game: MorpionGame = serde_json::from_str(serialized_str).unwrap();
        PyMorpion { game }
    }

    fn save(&self) -> String {
        serde_json::to_string(&self.game).unwrap()
    }

    fn get_current_player(&self) -> UserId {
        self.game.get_current_player()
    }

    fn get_next_player(&mut self) -> UserId {
        self.game.get_next_player()
    }

    fn play(&mut self, game_action: &PyMorpionAction) -> PyResult<()> {
        match self.game.play(&game_action.action) {
            Ok(a) => Ok(a),
            Err(e) => Err(map_error(e)),
        }
    }

    fn win(&self, last_game_action: &PyMorpionAction) -> bool {
        self.game.win(&last_game_action.action)
    }

    fn str(&self) -> String {
        self.game.str()
    }
}

fn map_error(err: MorpionError<char>) -> PyErr {
    match err {
        MorpionError::OutsideOfBoard(_) => PyErr::new::<OutsideOfBoard, _>(()),
        MorpionError::OccupiedPosition(_, _) => PyErr::new::<AlreadyOccupied, _>(()),
    }
}

/// A Python module implemented in Rust.
#[pymodule]
fn core(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<PyMorpion>()?;
    m.add_class::<PyMorpionAction>()?;
    m.add_class::<PyUserId>()?;

    Ok(())
}
