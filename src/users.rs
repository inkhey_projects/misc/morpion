use rand::Rng;
use serde::{Deserialize, Serialize};
use trait_set::trait_set;
trait_set! {
    pub(crate) trait TurnableItem = Copy;
}

pub type UserId = u128;

#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct Turn<I: TurnableItem> {
    players: Vec<I>,
    current_turn_nb: usize,
    current_item_index: usize,
}

impl<I: TurnableItem> Turn<I> {
    pub fn new(players: Vec<I>) -> Self {
        Turn {
            players,
            current_turn_nb: 1,
            current_item_index: 0,
        }
    }

    pub fn current(&self) -> I {
        self.players[self.current_item_index]
    }

    pub fn forward(&mut self, step: usize) -> I {
        self.current_item_index = (self.current_item_index + step) % self.players.len();
        self.current_turn_nb += 1;
        self.current()
    }

    #[allow(dead_code)]
    pub fn backward(&mut self, step: usize) -> I {
        if step > self.current_item_index {
            self.current_item_index = self.players.len() - (step - self.current_item_index);
        } else {
            self.current_item_index = (self.current_item_index - step) % self.players.len();
        }
        self.current_turn_nb += 1;
        self.current()
    }

    #[allow(dead_code)]
    pub fn random(&mut self) -> I {
        let mut rand_int: usize;
        loop {
            rand_int = rand::thread_rng().gen_range(0..self.players.len());
            if rand_int != self.current_item_index {
                break;
            }
        }
        self.current_item_index = rand_int;
        self.current_turn_nb += 1;
        self.current()
    }

    pub fn next(&mut self) -> I {
        self.forward(1)
    }

    #[allow(dead_code)]
    pub fn previous(&mut self) -> I {
        self.backward(1)
    }
}

#[cfg(test)]
mod tests {
    use crate::users::Turn;

    #[test]
    fn test_forward() {
        let mut turn = Turn::new(vec![1, 2, 3]);
        assert_eq!(turn.current(), 1);
        assert_eq!(turn.forward(1), 2);
        assert_eq!(turn.forward(2), 1);
        assert_eq!(turn.forward(3), 1);
        assert_eq!(turn.forward(2), 3);
        assert_eq!(turn.current(), 3);
    }

    #[test]
    fn test_backward() {
        let mut turn = Turn::new(vec![1, 2, 3]);
        assert_eq!(turn.current(), 1);
        assert_eq!(turn.backward(1), 3);
        assert_eq!(turn.backward(2), 1);
        assert_eq!(turn.backward(3), 1);
        assert_eq!(turn.backward(2), 2);
        assert_eq!(turn.current(), 2);
    }

    #[test]
    fn test_rand() {
        let mut turn = Turn::new(vec![1, 2]);
        assert_eq!(turn.current(), 1);
        assert_eq!(turn.random(), 2);
        assert_eq!(turn.random(), 1);
        turn = Turn::new(vec![1, 2, 3, 4, 5, 6, 7, 8, 9]);
        assert_eq!(turn.current(), 1);
        assert_ne!(turn.random(), 1);
    }
}
