use std::collections::HashMap;
use std::error::Error;
use std::io;

use clap::Parser;
use clap_num::number_range;
use i18n_embed::DesktopLanguageRequester;
use morpion::game::Game;
use unic_langid::LanguageIdentifier;

use morpion::i18n::{fl, localizer};
use morpion::morpion::game::MorpionGame;
use morpion::morpion::general::MorpionError::{OccupiedPosition, OutsideOfBoard};
use morpion::morpion::general::{Dimension, MorpionAction};
use morpion::users::UserId;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Name of the person to greet
    #[arg(short, long)]
    language: Option<String>,

    #[arg(short, long, default_value_t = 15)]
    rows: usize,

    #[arg(short, long, default_value_t = 15)]
    column: usize,

    #[arg(short, long, default_value_t = 5)]
    winning_row_length: u8,

    #[arg(short, long, default_value_t = 2, value_parser=nb_player_range)]
    nb_players: UserId,
}

fn nb_player_range(s: &str) -> Result<UserId, String> {
    number_range(s, 1, 5)
}

fn main() {
    let cli = Args::parse();
    let requested_languages = match cli.language {
        None => DesktopLanguageRequester::requested_languages(),
        Some(lang_code) => {
            let li: LanguageIdentifier = lang_code.as_str().parse().expect("Parsing failed.");
            vec![li]
        }
    };
    let library_localizer = localizer();
    if let Err(error) = library_localizer.select(&requested_languages) {
        eprintln!("Error while loading languages for library_fluent {error}");
    }
    let players_ids: Vec<UserId> = Vec::from_iter(0..cli.nb_players);
    let players_data = PlayerData {
        ids: players_ids,
        names: HashMap::from([
            (0, "Xenon"),
            (1, "Orange"),
            (2, "Violet"),
            (3, "Hibiscus"),
            (4, "Zoro"),
        ]),
        symbols: HashMap::from([(0, '🇽'), (1, '🔴'), (2, '🔶'), (3, '🌈'), (4, '🍏')]),
    };
    play(cli.column, cli.rows, cli.winning_row_length, &players_data);
}

struct PlayerData {
    ids: Vec<UserId>,
    names: HashMap<UserId, &'static str>,
    symbols: HashMap<UserId, char>,
}

enum GameOneInput {
    Val(usize),
    Quit,
}
enum GameActionInput {
    Position(Dimension),
    Quit,
    RestartTurn,
}

fn read_input() -> Result<String, Box<dyn Error>> {
    let mut input = String::new();
    io::stdin().read_line(&mut input)?;
    Ok(input)
}

fn read_game_input() -> Result<GameOneInput, Box<dyn Error>> {
    match read_input() {
        Ok(string_input) => Ok(match string_input.trim() {
            "stop" => GameOneInput::Quit,
            "quit" => GameOneInput::Quit,
            "q" => GameOneInput::Quit,
            value => {
                let int_value: usize = value.parse()?;
                GameOneInput::Val(int_value)
            }
        }),
        Err(error) => Err(error),
    }
}

fn player_input(
    players_data: &PlayerData,
    player_id: UserId,
    game: &MorpionGame,
) -> GameActionInput {
    println!("{}", fl!("sep"));
    println!(
        "{}",
        fl!(
            "user-turn",
            name = players_data.names[&player_id],
            p_char = format!("{}", players_data.symbols[&player_id])
        )
    );
    println!("{}", fl!("sep"));
    println!("{}", game.str());
    println!("{}", fl!("choose-position"));
    println!("{}\t:", fl!("horizontal-pos"));
    let row_input = read_game_input();
    let row: usize = match &row_input {
        Ok(GameOneInput::Quit) => {
            println!("{}", fl!("sep"));
            println!("{}", fl!("bye"));
            return GameActionInput::Quit;
        }
        Err(_) => {
            println!("{}", fl!("invalid-input"));
            return GameActionInput::RestartTurn;
        }
        Ok(GameOneInput::Val(val)) => *val,
    };
    println!("{}\t:", fl!("vertical-pos"));
    let row_column = read_game_input();
    let column = match &row_column {
        Ok(GameOneInput::Quit) => {
            println!("{}", fl!("sep"));
            println!("{}", fl!("bye"));
            return GameActionInput::Quit;
        }
        Ok(GameOneInput::Val(val)) => *val,
        Err(_) => {
            println!("{}", fl!("invalid-input"));
            return GameActionInput::RestartTurn;
        }
    };
    GameActionInput::Position((row, column))
}

fn play(column_nb: usize, row_nb: usize, winning_row_length: u8, players_data: &PlayerData) {
    println!("{}", fl!("game-title"));
    let mut game = MorpionGame::new(
        column_nb,
        row_nb,
        winning_row_length,
        players_data.ids.clone(),
    );
    let mut player_id: UserId = game.get_current_player();
    'game: loop {
        'player_turn: loop {
            let input: Dimension = match player_input(players_data, player_id, &game) {
                GameActionInput::Position(dimension) => dimension,
                GameActionInput::Quit => break 'game,
                GameActionInput::RestartTurn => continue 'player_turn,
            };
            let action = MorpionAction {
                position: input,
                board_box: players_data.symbols[&player_id],
            };
            match game.play(&action) {
                Ok(()) => {}
                Err(OutsideOfBoard(position)) => {
                    println!(
                        "{}",
                        fl!("fail-out-of-board", row = position.0, column = position.1)
                    );
                    continue 'player_turn;
                }
                Err(OccupiedPosition(position, player_char)) => {
                    println!(
                        "{}",
                        fl!(
                            "fail-already-used",
                            row = position.0,
                            column = position.1,
                            p_char = format!("{}", player_char)
                        )
                    );
                    continue 'player_turn;
                }
            }
            match game.win(&action) {
                true => {
                    println!("{}", game.str());
                    println!("{}", fl!("sep"));
                    println!("{}", fl!("win", name = players_data.names[&player_id]));
                    break 'game;
                }
                false => {
                    println!("{}", fl!("next-player-turn"));
                    break 'player_turn;
                }
            }
        }
        player_id = game.get_next_player();
    }
}
