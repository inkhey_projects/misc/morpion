pub type Action<A> = A;
pub type GameError<E> = E;

pub trait Game<A, E> {
    fn play(&mut self, action: &Action<A>) -> Result<(), GameError<E>>;
    fn win(&self, last_action: &Action<A>) -> bool;
    fn str(&self) -> String;
}
