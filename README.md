## Morpion

A Morpion game (Gomoku/5 in a row) in Rust and Python

This is early stage.

Todo :
- Real Gui/Tui
- Improve process (avoid error, etc)
- Packaging
- good practises
- Better test
- etc.


## Command-line usage:

Simple Rust CLI:
```bash
cargo run
```

Rust tests:
```bash
cargo test
```

# Dev tools (globally installed)
```
rye install pre-commit
rye install black --features colorama
rye install ruff
pre-commit --install
pre-commit run --all-files
```
Run python code:

```bash
rye sync --features dev
rye build
rye run devserver
rye run pytest
```