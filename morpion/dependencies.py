from typing import Annotated

import deal
from fastapi import Cookie, Depends
from starlette.responses import Response

from morpion.core import UserId
from morpion.database import room_database, user_database
from morpion.exceptions import RoomNotFound, Unauthorized, UncorrectedRoomState
from morpion.lib import Lobby, MorpionGame, Room
from morpion.settings import RoomIdParam, SettingsDep
from morpion.users import User
from morpion.utls import ServerCookie


@deal.has()
@deal.safe
async def get_user_id(
    cookie: Annotated[str | None, Cookie(alias="user_id")] = None
) -> UserId | None:
    if not cookie:
        return None
    return UserId.from_str(cookie)


@deal.has("io")
@deal.raises(Unauthorized)
@deal.raises(RoomNotFound)
async def get_user_from_cookie(
    user_id: Annotated[UserId | None, Depends(get_user_id)],
    response: Response,
    settings: SettingsDep,
) -> User:
    if user_id is None:
        raise Unauthorized()
    try:
        return user_database[str(user_id)]
    except KeyError as exc:
        ServerCookie.remove(user_id=user_id, response=response, settings=settings)
        headers = {"set-cookie": response.headers["set-cookie"]}
        raise Unauthorized(headers=headers) from exc


@deal.has()
@deal.raises(RoomNotFound)
async def get_any_room(room_id: RoomIdParam) -> Room:
    try:
        return room_database[room_id]
    except KeyError as exc:
        raise RoomNotFound(room_id=room_id) from exc


# Partial don`t work: https://github.com/tiangolo/fastapi/discussions/9744
# so we do have multiple fonctions


@deal.has()
@deal.raises(UncorrectedRoomState)
def get_lobby(room: Annotated[Room, Depends(get_any_room)]) -> Lobby:
    if isinstance(room, Lobby):
        return room
    else:
        raise UncorrectedRoomState(room=room, expected_type=MorpionGame)


@deal.has()
@deal.raises(UncorrectedRoomState)
def get_morpion(room: Annotated[Room, Depends(get_any_room)]) -> MorpionGame:
    if isinstance(room, MorpionGame):
        return room
    else:
        raise UncorrectedRoomState(room=room, expected_type=MorpionGame)
