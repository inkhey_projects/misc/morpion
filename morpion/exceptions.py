from dataclasses import dataclass
from http import HTTPStatus

from fastapi import HTTPException

from morpion.lib import Room


class MorpionHttpException(HTTPException):
    pass


@dataclass
class UncorrectedRoomState(MorpionHttpException):
    room: Room
    expected_type: type
    status_code: HTTPStatus = HTTPStatus.CONFLICT
    detail: str = "Invalid Room State"


@dataclass
class RoomNotFound(MorpionHttpException):
    room_id: str
    detail: str = "object used by the game server missing"
    status_code: HTTPStatus = HTTPStatus.NOT_FOUND


@dataclass
class Unauthorized(MorpionHttpException):
    status_code: HTTPStatus = HTTPStatus.UNAUTHORIZED
    detail: str = "No authentification enabled"
    headers: dict[str, str] | None = None
