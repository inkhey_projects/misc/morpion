from pydantic import BaseModel

from morpion.types import UserName, UserSymbol


class User(BaseModel):
    name: UserName
    symbol: UserSymbol
