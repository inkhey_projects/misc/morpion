from enum import StrEnum
from functools import lru_cache
from typing import Annotated, Literal

import deal
from fastapi import Depends, Path
from pydantic_settings import BaseSettings, SettingsConfigDict

from morpion.types import Seconds


class Settings(BaseSettings):
    USE_HTTPS: bool = True
    DOMAIN: str
    COOKIE_DURATION: Seconds = Seconds(1 * 60 * 60 * 24 * 30)  # A month
    SERVER_MODE: Literal["PROD", "TEST"] = "PROD"

    model_config = SettingsConfigDict(env_file=".env", extra="allow")


@lru_cache
@deal.pure
def get_settings() -> Settings:
    settings = Settings()
    return settings


SettingsDep = Annotated[Settings, Depends(get_settings)]
RoomIdParam = Annotated[str, Path(min_length=5, max_length=50)]
GameNameParam = str


class OpenApiTag(StrEnum):
    LOBBY_API = "️🕸️ Api»Lobby"
    MORPION_API = "️✖️ Api»Morpion"


TAG_METADATA = [
    {
        "name": OpenApiTag.LOBBY_API,
        "description": "Web API endpoint for lobby",
    },
    {
        "name": OpenApiTag.MORPION_API,
        "description": "Web API endpoint for Morpion game",
    },
]
