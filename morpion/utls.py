import deal
from starlette.responses import Response

from morpion.core import UserId
from morpion.settings import Settings
from morpion.types import Seconds


class ServerCookie:
    @deal.safe
    @deal.has("io")
    @staticmethod
    def create(user_id: UserId, response: Response, settings: Settings) -> None:
        ServerCookie._create(user_id, response, settings)

    @deal.has("io")
    @deal.safe
    @staticmethod
    def remove(user_id: UserId, response: Response, settings: Settings) -> None:
        ServerCookie._create(user_id, response, settings, max_age=Seconds(60))

    @deal.has("io")
    @deal.safe
    @staticmethod
    def _create(
        user_id: UserId, response: Response, settings: Settings, max_age: Seconds | None = None
    ) -> None:
        if max_age is None:
            max_age = settings.COOKIE_DURATION
        response.set_cookie(
            key="user_id",
            value=str(user_id),
            secure=settings.USE_HTTPS,
            httponly=True,
            max_age=max_age,
            samesite="strict",
            domain=settings.DOMAIN,
        )
