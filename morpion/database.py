from morpion.lib import Room
from morpion.users import User

RoomId = str

room_database: dict[RoomId, Room] = {}
user_database: dict[str, User] = {}
