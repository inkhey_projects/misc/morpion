import dataclasses
from typing import Protocol

import deal
from pydantic import BaseModel

from morpion.core import Morpion, MorpionAction, UserId
from morpion.types import ColumnNb, RowNb
from morpion.users import User


class Room(Protocol):
    name: str


@dataclasses.dataclass
class Lobby(Room):
    name: str
    players: list[UserId]

    @deal.has("local")
    @deal.safe
    def join(self, player: UserId) -> None:
        self.players.append(player)

    @deal.has("stdout")
    @deal.safe
    def speak(self, user: User, message: str) -> None:
        print(f"{user.name}{user.symbol} say {message}")


class MorpionMove(BaseModel):
    row: RowNb
    column: ColumnNb


class MorpionGame(Room):
    @deal.has()
    @deal.safe
    def __init__(self, lobby: Lobby, game: Morpion) -> None:
        self.name: str = lobby.name
        self.players: list[UserId] = lobby.players
        self.game = game

    @deal.has("stdout")
    @deal.safe
    def play(self, user: User, move: MorpionMove) -> None:
        self.game.play(MorpionAction(board_box=user.symbol, position=(move.column, move.row)))
        print(self.game.str())

    @deal.pure
    def finish(self) -> Lobby:
        return Lobby(name=self.name, players=self.players)
