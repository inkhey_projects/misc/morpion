from typing import Annotated, Final, NewType

from annotated_types import Interval, Len
from pydantic import NonNegativeInt

# Unsigned Int 8
MAX_U8: Final = 2**8 - 1
MIN_U8: Final = 0
U8 = NewType("U8", Annotated[int, Interval(ge=MIN_U8, le=MAX_U8)])

# Unsigned Int 64
MAX_U64: Final = 2**64 - 1
MIN_U64: Final = 0
U64 = NewType("U64", Annotated[int, Interval(ge=MIN_U64, le=MAX_U64)])

# Unsigned Int 128
MAX_U128: Final = 2**128 - 1
MIN_U128: Final = 0
U128 = NewType("U128", Annotated[int, Interval(ge=MIN_U128, le=MAX_U128)])

# Char-like Type
MAX_CHAR_LENGTH: Final = 1
MIN_CHAR_LENGTH: Final = 1
Char = NewType("Char", Annotated[str, Len(1, 1)])

# Time in second
Seconds = NewType("Seconds", NonNegativeInt)

# Move
ColumnNb = NewType("ColumnNb", U64)
RowNb = NewType("RowNb", U64)

# User
MIN_NAME_LENGTH: Final = 3
MAX_NAME_LENGTH: Final = 100
UserName = NewType("UserName", Annotated[str, Len(MIN_NAME_LENGTH, MAX_NAME_LENGTH)])
UserSymbol = NewType("UserSymbol", Char)
