import datetime
from collections.abc import AsyncGenerator
from contextlib import asynccontextmanager
from typing import Literal
from zoneinfo import ZoneInfo

import deal
from fastapi import FastAPI
from pydantic import BaseModel
from starlette.requests import Request

from morpion import api
from morpion.settings import TAG_METADATA, Settings, SettingsDep, get_settings

SERVICE_NAME = "Morpion"
DESCRIPTION = "A nice morpion game server"


@asynccontextmanager
@deal.pure
async def lifespan(app: FastAPI) -> AsyncGenerator[None, None]:
    deal.disable()
    settings = get_settings()
    if settings.SERVER_MODE == "PROD":
        deal.disable(permament=True)
    else:
        deal.enable()
    yield


app = FastAPI(
    lifespan=lifespan,
    title=SERVICE_NAME,
    summary=DESCRIPTION,
    openapi_tags=TAG_METADATA,
)

app.include_router(api.web_api)


class HomeResponse(BaseModel):
    service: str
    api_type: str
    api_url: str
    docs: str
    redoc: str
    date: datetime.datetime
    service_mode: str
    settings: Settings | Literal["Hidden"]


@deal.has("time")
@deal.safe
@app.get("/")
async def home(request: Request, settings: SettingsDep) -> HomeResponse:
    return HomeResponse(
        service=SERVICE_NAME,
        api_type="REST",
        api_url="/api",
        docs=str(request.url_for("swagger_ui_html")),
        redoc=str(request.url_for("redoc_html")),
        service_mode=settings.SERVER_MODE,
        settings=settings if settings.SERVER_MODE != "PROD" else "Hidden",
        date=datetime.datetime.now(tz=ZoneInfo("Europe/Paris")),
    )
