# ╔═════════════════════════════════════════════╗
# ║          🐍 REST API for Morpion game       ║
# ╚═════════════════════════════════════════════╝
from contextlib import nullcontext
from http import HTTPStatus
from typing import Annotated

import deal
from annotated_types import Ge, Len
from fastapi import APIRouter, Depends
from pydantic import BaseModel
from sse_starlette.sse import EventSourceResponse
from starlette.requests import Request
from starlette.responses import Response
from typing_extensions import TypedDict

from morpion.core import Morpion, UserId
from morpion.database import room_database, user_database
from morpion.dependencies import get_lobby, get_morpion, get_user_from_cookie, get_user_id
from morpion.lib import Lobby, MorpionGame, MorpionMove
from morpion.settings import OpenApiTag, RoomIdParam, SettingsDep
from morpion.types import U8, ColumnNb, RowNb
from morpion.users import User
from morpion.utls import ServerCookie

MESSAGE_STREAM_DELAY = 1  # second
MESSAGE_STREAM_RETRY_TIMEOUT = 15000  # millisecond


class Message(BaseModel):
    detail: str


web_api = APIRouter(
    prefix="/api",
)

lobby_route = APIRouter(
    prefix="/lobby",
    tags=[OpenApiTag.LOBBY_API],
)


class SSEResponse(EventSourceResponse):
    media_type = "text/event-stream"


@deal.pure
@web_api.get(path="/events/{room_id}", response_class=SSEResponse, response_model=Message)
async def message_stream(request: Request) -> Message:
    return Message(detail="super")


class JoinResponse(BaseModel):
    user_id: str
    room_id: str
    created: bool


@deal.has("io")
@deal.safe
@lobby_route.post("/{room_id}/join")
async def join(
    response: Response,
    settings: SettingsDep,
    room_id: RoomIdParam,
    user_id: Annotated[UserId | None, Depends(get_user_id)],
) -> dict[str, str | bool]:
    """
    Join room and get data from it
    """
    if not user_id:
        user_id = UserId()
        with nullcontext():
            user_id_str = str(user_id)
            user_database[user_id_str] = User(
                name=user_id_str,
                symbol="🔥",
            )
            ServerCookie.create(
                user_id=user_id,
                response=response,
                settings=settings,
            )
    if _room_data := room_database.get(room_id):
        return {"user_id": str(user_id), "room_id": room_id, "created": False}
    else:
        room_database[room_id] = Lobby(room_id, players=[user_id])
        return {"user_id": str(user_id), "room_id": room_id, "created": True}


class MessageBody(BaseModel):
    message: Annotated[str, Len(1, 140)]


@deal.has("io")
@deal.safe
@lobby_route.post("/{room_id}/speak", status_code=HTTPStatus.NO_CONTENT)
async def speak(
    user: Annotated[User, Depends(get_user_from_cookie)],
    lobby: Annotated[Lobby, Depends(get_lobby)],
    message_body: MessageBody,
) -> None:
    lobby.speak(user, message_body.message)


morpion_route = APIRouter(
    prefix="/morpion",
    tags=[OpenApiTag.MORPION_API],
)


class CreateMorpionBody(BaseModel):
    column_nb: Annotated[ColumnNb, Ge(3)]
    row_nb: Annotated[RowNb, Ge(3)]
    winning_row_length: Annotated[U8, Ge(3)]


class MorpionGameInfos(TypedDict):
    pass


@deal.pure
@morpion_route.get("/{room_id}")
async def morpion_state(
    _user: Annotated[User, Depends(get_user_from_cookie)],
    morpion_game: Annotated[MorpionGame, Depends(get_morpion)],
) -> MorpionGameInfos:
    morpion_game.game.get_current_player()
    return {}


@deal.has("io")
@deal.safe
@morpion_route.post("/{room_id}/start", status_code=HTTPStatus.NO_CONTENT)
async def start(
    _user: Annotated[User, Depends(get_user_from_cookie)],
    lobby: Annotated[Lobby, Depends(get_lobby)],
    room_id: RoomIdParam,
    body: CreateMorpionBody,
) -> None:
    game = Morpion(
        column_nb=body.column_nb,
        row_nb=body.row_nb,
        winning_row_length=body.winning_row_length,
        board_data=None,
        py_players=lobby.players,
    )
    room_database[room_id] = MorpionGame(lobby, game)


@deal.has("io")
@deal.safe
@morpion_route.post("/{room_id}/play", status_code=HTTPStatus.NO_CONTENT)
async def play(
    user: Annotated[User, Depends(get_user_from_cookie)],
    morpion_game: Annotated[MorpionGame, Depends(get_morpion)],
    move: MorpionMove,
) -> None:
    morpion_game.play(user, move)


web_api.include_router(lobby_route)
web_api.include_router(morpion_route)
