import string
from http import HTTPStatus
from typing import Annotated
from urllib.parse import quote

import deal
import pytest
from annotated_types import Len
from fastapi import FastAPI
from starlette.testclient import TestClient
from syrupy.assertion import SnapshotAssertion
from syrupy.filters import props

import morpion.settings
from morpion.api import JoinResponse
from morpion.app import app as morpion_app
from morpion.settings import Settings

ALLOWED_URL_CHAR = string.digits + string.ascii_letters + "-"

deal.disable()


@deal.pure
@pytest.fixture
def app() -> FastAPI:
    return morpion_app


@deal.pure
@pytest.fixture
def client(app: FastAPI) -> TestClient:
    return TestClient(app)


@deal.pure
def settings_test() -> Settings:
    return Settings(_env_file="./tests/.test_env")


@deal.pure
def false_prod_settings() -> Settings:
    return Settings(_env_file="./tests/.false_prod_env")


@deal.pure
@pytest.fixture
def test_settings_app(app: FastAPI) -> FastAPI:
    app.dependency_overrides[morpion.settings.get_settings] = settings_test
    return app


@deal.pure
@pytest.fixture
def test_false_prod_app(app: FastAPI) -> FastAPI:
    app.dependency_overrides[morpion.settings.get_settings] = false_prod_settings
    return app


class TestApi:
    @deal.has()
    @deal.raises(AssertionError)
    def test_root_nominal_case(
        self,
        snapshot: SnapshotAssertion,
        test_false_prod_app: FastAPI,  # noqa: ARG002
        client: TestClient,
    ) -> None:
        response = client.get("/")
        assert response.status_code == HTTPStatus.OK, response.text
        assert response.json() == snapshot(exclude=props("date", "1"))

    @deal.has()
    @deal.raises(AssertionError)
    def test_root_dev_env(
        self,
        snapshot: SnapshotAssertion,
        test_settings_app: FastAPI,  # noqa: ARG002
        client: TestClient,
    ) -> None:
        response = client.get("/")
        assert response.status_code == HTTPStatus.OK, response.text
        assert response.json() == snapshot(exclude=props("date", "1"))

    @deal.raises(AssertionError, TypeError)
    @deal.has()
    @pytest.mark.parametrize(
        "room_id,message",
        [
            ["my_super_room", "Génial !"],
            ["my super room", "Bonvenon !"],
            ["génial?!", "😂"],
            ["¿" * 50, "_" * 140],
            ["🍉🌏🍊🍋🥝", "…"],
        ],
    )
    def test_lobby__ok__nominal_case(
        self,
        test_settings_app: FastAPI,  # noqa: ARG002
        client: TestClient,
        room_id: Annotated[str, Len(min_length=5, max_length=50)],
        message: str,
    ) -> None:
        _response = client.post(f"/api/lobby/{quote(room_id)}/join")
        assert _response.status_code == HTTPStatus.OK, _response.text
        response = JoinResponse(**_response.json())
        user_id: str = response.user_id
        assert user_id
        assert response.room_id == room_id
        assert response.created is True

        _response = client.post(f"/api/lobby/{quote(room_id)}/join")
        assert _response.status_code == HTTPStatus.OK, _response.text
        response = JoinResponse(**_response.json())
        assert response.user_id == user_id
        assert response.room_id == room_id
        assert response.created is False
        _response = client.post(url=f"/api/lobby/{quote(room_id)}/speak", json={"message": message})
        assert _response.status_code == HTTPStatus.NO_CONTENT, _response.text

    @deal.raises(AssertionError, TypeError)
    @deal.has()
    @pytest.mark.parametrize(
        "room_id,column_nb,row_nb,winning_row_length",
        [
            ["tictactoe", 3, 3, 3],
            ["gomoku", 15, 15, 5],
            ["big_room", 100, 150, 8],
        ],
    )
    def test_morpion_game(
        self,
        test_settings_app: FastAPI,  # noqa: ARG002
        client: TestClient,
        room_id: Annotated[str, Len(min_length=5, max_length=50)],
        column_nb: int,
        row_nb: int,
        winning_row_length: int,
    ) -> None:
        _response = client.post(f"/api/lobby/{quote(room_id)}/join")
        assert _response.status_code == HTTPStatus.OK, _response.text
        JoinResponse(**_response.json())
        _response = client.post(
            f"/api/morpion/{quote(room_id)}/start",
            json={
                "column_nb": column_nb,
                "row_nb": row_nb,
                "winning_row_length": winning_row_length,
            },
        )
        assert _response.status_code == HTTPStatus.NO_CONTENT, _response.text
